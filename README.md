

![Application UI](https://cloud.githubusercontent.com/assets/15085641/17646353/587e60d0-61bd-11e6-9403-82437ee3a6e6.png)


- Click here for see the [Live demo](http://media-gallery.herokuapp.com).


# Technology used
* [React](https://facebook.github.io/react/) as the core infrastructure.
* [Redux](https://github.com/reactjs/redux) for state management.
* [Redux-saga](https://github.com/yelouafi/redux-saga) for handling async tasks with agility.


8. Deploy app on Heroku.



